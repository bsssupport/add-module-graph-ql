<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_AddModuleGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\AddModuleGraphQl\Observer;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CheckoutCart;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

/**
 * Class AddModuleGraphQl
 *
 * @package Bss\AddModuleGraphQl\Observer
 */
class AddModuleGraphQl implements ObserverInterface
{
    /**
     * @var string
     */
    protected $productIdsUsed = ",";
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var CheckoutCart
     */
    protected $checkCart;

    /**
     * AddModuleGraphQl constructor.
     *
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param CheckoutCart $checkCart
     */
    public function __construct(
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        CheckoutCart $checkCart
    ) {
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->checkCart = $checkCart;
    }

    /**
     * Add to cart module graphql
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            $items = $observer->getEvent()->getItems();
            foreach ($items as $item) {
                $productId = $item->getProductId();
                $this->productIdsUsed = $this->productIdsUsed .$productId.",";
                $product = $this->productRepository->getById($item->getProductId(), false);
                $productIds = $this->getProductIdsGraphQl($product);
                if ($productIds) {
                    foreach ($productIds as $productIdGraphQl) {
                        if(is_numeric(strpos($this->productIdsUsed,",".$productIdGraphQl.","))){
                            continue;
                        }
                        $requestInfo = [
                            "qty" => $item->getQtyToAdd(),
                            "product" => $productIdGraphQl
                        ];
                        $productGraphQl = $this->productRepository->getById($productIdGraphQl, false);
                        $this->checkCart->addProduct($productGraphQl, $requestInfo);
                    }
                }
            }
        } catch (Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }

    /**
     * Get product ids of module graphql
     *
     * @param ProductInterface $product
     * @return false|string[]
     */
    public function getProductIdsGraphQl($product)
    {
        $productIds = $product->getModuleGraphQl();
        if (isset($productIds)) {
            return explode(',', $productIds);
        }
        return $productIds;
    }
}
