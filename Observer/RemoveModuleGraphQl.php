<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_AddModuleGraphQl
 * @author     Extension Team
 * @copyright  Copyright (c) 2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AddModuleGraphQl\Observer;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magento\Checkout\Model\Cart as CustomerCart;

/**
 * Class RemoveModuleGraphQl
 *
 * @package Bss\AddModuleGraphQl\Observer
 */
class RemoveModuleGraphQl implements ObserverInterface
{
    /**
     * @var string
     */
    protected $productIdsUsed = ",";
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;
    /**
     * @var CustomerCart
     */
    protected $cart;

    /**
     * RemoveModuleGraphQl constructor.
     *
     * @param CustomerCart $cart
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        CustomerCart $cart,
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        CheckoutSession $checkoutSession
    ) {
        $this->cart = $cart;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     *  cart module graphql
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        try {
            $item = $observer->getEvent()->getQuoteItem();
            $productId = $item->getProductId();
            $this->productIdsUsed = $this->productIdsUsed . $productId . ",";
            $product = $this->productRepository->getById($item->getProductId(), false);
            $productIds = $this->getProductIdsGraphQl($product);
            if ($productIds) {
                foreach ($productIds as $productIdGraphQl) {
                    $allItems = $this->checkoutSession->getQuote()->getAllVisibleItems();
                    foreach ($allItems as $item) {
                        if($item->getProduct()->getId() == $productIdGraphQl) {
                            $itemId = $item->getItemId();
                            $this->cart->removeItem($itemId);
                        }
                    }
                }
            }
        } catch (Exception $exception) {
            $this->logger->critical($exception->getMessage());
        }
    }

    /**
     * Get product ids of module graphql
     *
     * @param ProductInterface $product
     * @return false|string[]
     */
    public function getProductIdsGraphQl($product)
    {
        $productIds = $product->getModuleGraphQl();
        if (isset($productIds)) {
            return explode(',', $productIds);
        }
        return $productIds;
    }
}
